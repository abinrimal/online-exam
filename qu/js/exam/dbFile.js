(function() {
  	'use strict';

  	var db;

  	const answerData = [
  	   { id: "01", name: "Gopal K Varma", age: 35, email: "contact@tutorialspoint.com" },
  	   { id: "02", name: "Prasad", age: 24, email: "prasad@tutorialspoint.com" }
  	];

  	function add(id, inputType, optionId){
  		var request = db.transaction(["exam-answer"], "readwrite")
  		   			  .objectStore("exam-answer")
  		   			  .add({ id: id, inputType: inputType, optionId: optionId });
  		   
		   	request.onsuccess = function(event) {
		     	alert("Prasad has been added to your database.");
		   	};
  		   
		   	request.onerror = function(event) {
		      	alert("Unable to add data\r\nPrasad is already exist in your database! ");
		   	}
  	}

  	function read() {
  	   var transaction = db.transaction(["exam-answer"]);
  	   var objectStore = transaction.objectStore("exam-answer");
  	   var request = objectStore.get("00-03");
  	   
  	   request.onerror = function(event) {
  	      alert("Unable to retrieve daa from database!");
  	   };
  	   
  	   request.onsuccess = function(event) {
  	      
  	      if(request.result) {
  	         alert("Name: " + request.result.name + ", Age: " + request.result.age + ", Email: " + request.result.email);
  	      } else {
  	         alert("Kenny couldn't be found in your database!");  
  	      }
  	   };
  	}

  	function readAll() {
  	   var objectStore = db.transaction("exam-answer").objectStore("exam-answer");
  	   
  	   objectStore.openCursor().onsuccess = function(event) {
  	      var cursor = event.target.result;
  	      
  	      if (cursor) {
  	         alert("Name for id " + cursor.key + " is " + cursor.value.name + ", Age: " + cursor.value.age + ", Email: " + cursor.value.email);
  	         cursor.continue();
  	      } else {
  	         alert("No more entries!");
  	      }
  	   };
  	}

  	function remove() {
  	   var request = db.transaction(["exam-answer"], "readwrite")
  	   .objectStore("exam-answer")
  	   .delete("02");
  	   
  	   request.onsuccess = function(event) {
  	      alert("prasad entry has been removed from your database.");
  	   };
  	}

 	//check for support
  	if (!('indexedDB' in window)) {
    	console.log('This browser doesn\'t support IndexedDB');
    	return;
  	}

  	var dbPromise = indexedDB.open('test-db1', 3);

  	dbPromise.onerror = function(event){
  		console.log("couldn't open db.");
  	}
  	dbPromise.onsuccess = function(event){
  		console.log("DB opened!");
  		db = event.traget.result;
  	}


})();