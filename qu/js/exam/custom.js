function Question(q){
	this.question = q.question;
	this.answerType = q.ansType;
	this.options = q.options;
	this.img = q.img;
	this.name = q.name;
}

Question.prototype.forEachOption = function(callback, context){
	this.options.forEach(callback, context);
}

