$(document).ready(function(){
		// Div containers
		var form = $("#form");
		// Question number
		var qNum = 0;

		// Controls the page to show on the screen
		function control(num){

			// Refreshing question number/page with every change to the question
			qNum = "Question : " + (num + 1) + " of " + totalQ;
			$("#qNum").text(qNum);

			var filter_name = "unique-filter-" + num;

			if(num < sheet.length){

				$("#form").find(".exam-quest").each(function(){

					if($(this).attr("data-filter") == filter_name ){
						$(this).removeClass("hide").addClass("disp");
						$(".leadBtn").each(function(e){
							if(e == num){
								$(this).addClass("active");
							}else{
								$(this).removeClass("active");
							}
						});
					}else{
						$(this).removeClass('disp').addClass("hide");
					}
				});



			}else{
				alert("Error in question indexing!!");
			}
		}
		
		// Append objects to the html document
		function onLoadFun(){

			for(var j = 0; j < sheet.length; j++){

				// selecting object by indexing number to the array
				var a = sheet[j];
				var att = "#opt-"+j;


				var questionHTML = [
					"<div class='row exam-quest' id='opt-"+ j +"' data-filter='unique-filter-" + j + "'>" +
						"<div class='col-lg'>" + 
							"<div class='quest-text'>" 
								+ (j+1) + ". " + a.question +
							"</div>" + 
						"</div>" + 
						"<div class='col-lg'>"+
							"<div class='options'>"
				];

				form.append(questionHTML);


				var searchOpt = $(att).children().find(".options");

				if(a.ansType == 'checkbox' || a.ansType == 'radio'){
					// Looping for multiple choice answers
					for(var i = 0; i < a.options.length; i++){
						
						var choiceHTML = [
							"<div data-target='no' class='choice-design'>" + 
								"<span class='check-radio-contain'>" +
									"<input type='" + a.ansType + "' id='" + a.id + "-" + i + "' name='" + a.id + "[]' data-target=" + j + ">"+
								"</span>" +
								"<label for=" + a.id + i + "> " + a.options[i] + "</label>" + 
							"</div>"
						];

						searchOpt.append(choiceHTML);

					}
					searchOpt.append("</div></div></div>");
				}else if(a.ansType == "drag&drop"){

					var row = [
						"<div class='row draggable-container'>"+
							"<div class='col-lg-5' id='draggable-img'>" +
								"<ul class='bt-bt'>"
					];
					searchOpt.append(row);

					// Looping for multiple choice answers
					for(var i = 0; i < a.options.length; i++){
						
						var choiceHTML = [
							"<li class='drag-holder'>" + 
								'<img src="' + a.options[i] + '">' +
							"</li>"
						];

						searchOpt.children().find(".bt-bt").append(choiceHTML);

					}
					var endCol = [
						"<div class='col-lg-7'>"+
							"<ul class='contain-drag-img'></ul>" +
							"<input type='hidden' class='drag-input-data' id='" + a.id + "-" + i + "' name='" + a.id + "' data-target='" + j + "'>"+
						"</div>"
					];
					searchOpt.find(".draggable-container").append(endCol);

					// var container = [
					// 		"<div class='col-lg-6'>"+
					// 			"<div class='contain-img'></div>" +
					// 			"<input type='hidden' id='" + a.id + "-" + i + "' name='" + a.id + "' data-target='" + j + "'>"+
					// 		"</div>"
					// ]; 
					// $(".draggable-container").append(container);

				}
				
				

				if(a.ansType == "img_blanks"){

					var blanksImgHTML = [
						"<img src='" + a.img + "'>" +
						"<div>" +
							"<input type='text' class='bord-btm' name='" + a.id + "' placeholder='Fill Here' data-target=" + j + ">" +
						"</div>"
					];

					searchOpt.append(blanksImgHTML);

				}else if(a.ansType == "fill_blanks"){
					$(att).children().find(".blanks_here").addClass("fill_blanks-"+j);
					var blanksFillHTML = [
						"<input type='text' class='bord-btm' name='" + a.id + "' placeholder='Fill Here' data-target=" + j + ">"
					];

					$(".fill_blanks-"+j).append(blanksFillHTML);

				}

				var ctrlBtn = (j<9)? "0" : "";
				var leadBtn = $("<button>"+ctrlBtn + (j+1)+"</button>").attr({
					type: "button",
					class: "leadBtn"
				});
				// var leadBtn = [
				// 	"<button class='leadBtn'>"+ ctrlBtn + (j+1) +"</button>"
				// ];

				$(".top-btn-holder").append(leadBtn);

			}

			
		}

		// Loading the document with json files and etc
		onLoadFun();

		// Initializing the page to 1 page on load of the document
		control(currentQ);

		// Event on click of next button
		$("#next").click(function(){
			if(currentQ >= (totalQ-1)){
				$("#next").attr('disabled', 'disabled');
			}else{
				$("#prev").removeAttr('disabled');
				control(++currentQ);
			}
		});
			
		// Event on click of prev button
		$("#prev").click(function(){
			if(currentQ <= 0){
				$("#prev").attr('disabled','disabled');
			}else{
				$("#next").removeAttr('disabled');
				control(--currentQ);
			}
		});

		// Event on click of end button
		$("#end").click(function(){
			$('.alert-end').addClass('is-visible');
		});

		// Change the page to the clicked button number 
		$(".leadBtn").click(function(){
			currentQ = $(this).text() - 1;
			control(currentQ);
		});

		// Check for the given match between buttons text and value of the input attribute
		function changeBg(comp, num){
			$(".leadBtn").each(function(e){
				if(e == num){
					if(comp == true){
						$(this).addClass("complete");
					}else if(comp == false){
						$(this).removeClass("complete");
					}
				}
			});
		}

		// Button background color change on input or selection of an answer
		$("input").on('change',function(){

			// takes input fields attribute value
			var s = $(this).attr("data-target");

			// In case of checkbox and radio input field
			if($(this).is(":checked")){
				
			// For if you want to save user input to localStorage or indexedDB 
				// // Variables and others to store input data to DB
				// var opt_ids;
				// var id;
				// opt_ids = $(this).attr('id');
				// var id = $(this).attr('name').split('[')[0];

				// if($(this).is(':checkbox')){
				// 	addToDB(id, "checkbox", opt_ids);
				// }else{
				// 	// store the checked input to db
				// 	addToDB(id, "radio", opt_ids);
				// }
				
				changeBg(true, s);
				$(this).parent().parent().attr("data-target","yes");

			}else{
				var remOrNo = [];
				$(this).parent().parent().attr("data-target","no");

				// push 0 or 1 to the array according to the value of data-target
				$(this).parent().parent().siblings().each(function(){
					if($(this).attr("data-target") == "no"){
						remOrNo.push(0);
					}else if($(this).attr("data-target") == "yes"){
						remOrNo.push(1);
					}
				});

				// Check if the condition is true
				function returnCond(arr){
					return arr < 1;
				}

				// Check if every element in the array returns true from the returnCond() function
				if(remOrNo.every(returnCond) == true){
					
					// Remove the data that is being unchecked
					// addToDB();

					changeBg(false, s);
				}else{
					return 1;
				}
				remOrNo = [];
			}

			// Saving input values in indexed DB

			
		});

		$(".bord-btm").change(function(){

			// takes input fields attribute value
			var s = $(this).attr("data-target");
			
			if($(this).val() > ''){
				changeBg(true, s);
			}else{
				changeBg(false, s);
			}
		});


	});