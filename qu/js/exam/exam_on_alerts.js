// 
$(".alert-exit .alert-confirm").click((e) => {
	e.preventDefault();
	isIn = true;
	$('body').fullscreen();
	$(".alert-exit").removeClass('is-visible');
	$("#end").css('display','block');
	stopIt();
});

$(".alert-exit .alert-cancel").click((e) => {
	e.preventDefault();
	$('.alert-exit').removeClass('is-visible');
	alert("You have exited from the exam.");
	$("#end").css('display','none');
	gotIn = false;
});
	

$(".alert .alert-confirm").click((e) => {
	e.preventDefault();
  	$('body').fullscreen();
  	$(".alert").removeClass("is-visible");
  	$("#end").css('display','block');
  	gotIn = true;
});

$('.alert-confirm-end').click((e) => {
	e.preventDefault();
	$.fullscreen.exit();
	$(".alert-end").removeClass("is-visible");
	isIn = false;
	$("#end").css('display','none');
	alert("Your exam has ended.");
});

$(".alert-close, .alert-cancel").click((e) => {
  e.preventDefault();
  $(".alert").removeClass("is-visible");
  $(".alert-end").removeClass("is-visible");
  $(".alert-exit").removeClass("is-visible");
});

$('.alert-warning-user .alert-confirm').click((e) => {
	e.preventDefault();
	$(".alert").removeClass("is-visible");
	$(".alert-end").removeClass("is-visible");
	$(".alert-exit").removeClass("is-visible");
	$('.alert-warning-user').removeClass('is-visible');
	console.log("Exam cheated.");
});