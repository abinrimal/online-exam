// stores true when the screen goes fullscreen on load of the window
var gotIn = false;

var isIn = true;

// function to stop the exam and store all the form input
function examStop(){

}

// Interval to check if the window is in fullscreen mode of not
setInterval(function(){
	if(gotIn == true){
		if(isIn == true){
			if($.fullscreen.isFullScreen() == false){
				$('.alert-exit').addClass('is-visible');
				isIn = false;
				countExamStop();
			}
		}
	}
},1000);

// stores countdown for fullscreen exit
var intervalStopTime;

// starts countdown when user exits fullscreen mode
function countExamStop(){
	var time = 19;
	intervalStopTime = setInterval(function(){
						if(time == 0){
							$("#end").css('display','none');
							alert("Your exam has now ended.");
							$('.alert-exit').removeClass('is-visible');
							stopIt();
						}
						$(".exam-stop-time").text(time + " seconds");
						time--;
					},1000);

}
// clears the timeinterval
function stopIt(){
	clearInterval(intervalStopTime);
	$(".exam-stop-time").text("20 seconds");
}