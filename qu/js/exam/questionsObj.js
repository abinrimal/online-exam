var totalQ;
var currentQ;
var sheet = [
	{
		question: "Which two of the following numbers have a product that is between –1 and 0? Indicate both of the numbers.",
		ansType: "checkbox",
		options: [
			"-20", "-10", "2<sup>-4</sup>", "3<sup>2</sup>"
		],
		img: "",
		id: "1"
	},
	{
		question: "Traffic Signal to Stop the vehicle(s).",
		ansType: "radio",
		options: [
			"Green", "Red", "Yellow", "Purple", "None of the above"
		],
		img: "",
		id: "2"
	},
	{
		question: "Which of the following integers are multiples of both 2 and 3? Indicate all such integers.",
		ansType: "checkbox",
		options: [
			8, 9, 12, 18, 24
		],
		img: "",
		id: "3"
	},
	{
		question: "Watch the image below and explain give an appropriate answer.",
		ansType: "img_blanks",
		options: [],
		img: "cat.jpg",
		id: "4"
	},
	{
		question: "Each employee of a certain company is in either Department X or Department Y, and there are more than twice as many employees in Department X as in Department Y. The average (arithmetic mean) salary is $25,000 for the employees in Department X and $35,000 for the employees in Department Y. Which of the following amounts could be the average salary for all of the employees of the company?",
		ansType: "checkbox",
		options: [
			"$26,000", "$28,000", "$29,000", "$30,000", "$36,000"
		],
		img: "",
		id: "5"
	},
	{
		question: "RAM (Random Access Memory) is a <span class='blanks_here'></span> memory.",
		ansType: "fill_blanks",
		options: [],
		img: "",
		id: "6"
	},
	{
		question: "Drag and drop to the specific part.",
		ansType: "drag&drop",
		options: [
			"http://images.all-free-download.com/images/graphiclarge/movie_icons_collection_by_icons8_6825006.jpg",
			"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT4YNA-f1D7uyGl4xaBYKz5TT7NeOLCjSwVJteJLtrzlNO-oKN5",
			"https://image.flaticon.com/sprites/new_packs/206852-profession-avatars.png"
		],
		img: "",
		id: "7"
	}

];


totalQ = sheet.length;
currentQ = 0;