// select the option on which type of question set to create
$("#question_type").change(function(){
	var opt_num = 0;

	switch($(this).val()){
		case "checkbox":{
			$("#chk-question").siblings().hide();
			if(!($("#chk-question .chk-options").children().length > 0)){
				// opt_num = prompt("How many checkbox options does the question have?");
				// if(opt_num > 1){
				// 	makeCheckbox(opt_num, "clear");
				// }else{
				// 	makeCheckbox(2, "clear");
				// }
				makeCheckbox(4, "clear");
			}else{
				$("#chk-question").fadeIn(500);
			}
			break;
		}
		case "radio":{
			$("#rad-question").siblings().hide();
			if(!($("#rad-question .rad-options").children().length > 0)){
				// opt_num = prompt("How many radio options does the question have?");
				// if(opt_num > 0){
				// 	makeRadio(opt_num, "clear");
				// }else{
				// 	makeRadio(2, "clear");
				// }
				makeRadio(4, "clear");
			}else{
				$("#rad-question").fadeIn(500);
			}
			break;
		}
		case "img_blanks":{
			$("#ig-question").siblings().hide();
			if(!($("#ig-question .ig-options").children().length > 0)){
				makeImgBlanks("clear");
			}else{
				$("#ig-question").fadeIn(500);
			}
			break;
		}
		case "fill_blanks": {
			$("#fith-question").siblings().hide();
			if(!($("#fith-question .fith-options").children().length > 0)){
				// makeFithBlanks();
				$("#fith-question").fadeIn(500);
			}else{
				$("#fith-question").fadeIn(500);
			}
			break;
		}
		case "drag&drop": {
			$("#idrag-question").siblings().hide();
			if(!($("#idrag-question .idrag-options").children().length > 0)){
				makeDragDrop();
				$("#idrag-question").fadeIn(500);
			}else{
				$("#idrag-question").fadeIn(500);
			}
			break;
		}
		case "audio": {
			$("#audio-question").siblings().hide();
			if(!$("#audio-question .audio-options").children().length > 0){
				makeAudioFile();
				$("#audio-question").fadeIn(500);
			}else{
				$("#audio-question").fadeIn(500);
			}
			break;
		}
		default:{
			console.log("default");
			break;
		}
	}

});


// for generating image to show on choose of image
function readURL(input) {

  	if (input.files && input.files[0]) {
	    var reader = new FileReader();

	    reader.onload = function(e) {
	      var img = "<img src='" + e.target.result + "' class='guess_img_style'/>";
	      $(input).siblings(".guess_img_holder").children(".guess_img_style").remove();
	      $(input).siblings(".guess_img_holder").append(img);
	    }

    	reader.readAsDataURL(input.files[0]);
  	}
}

// in-case of change in image guess question set
$("#ig-question").on("change", "input", function(){
	var u = readURL(this);
});
$("#idrag-question").on("change", "input", function(){
	var u = readURL(this);
});


// Function to add a new question dynamically
$(".addAnoQuest").click(function(){

	// creating a close to append it as a new question
	var a = $(this).siblings(".set-container").children(".question-set").last().clone();

	// hide the question during append and add a fadein effect
	$(a).hide().appendTo($(this).siblings(".set-container")).fadeIn(400);
	// clear out all the values inside input and textarea
	$(this).siblings(".set-container").children(".question-set").last().find('.row').first().find("input, textarea").each(function(){
		$(this).val(null);
	});
	// clear the image generated on a div
	$(this).siblings(".set-container").children(".question-set").last().find(".guess_img_holder").each(function(){
		$(this).find(".guess_img_style").remove();
	});
});

$(".addAnoQuest-1").click(function(){
	// creating a close to append it as a new question
	var a = $(this).parent().find(".question-set").last().clone();

	// hide the question during append and add a fadein effect
	$(a).hide().insertAfter($(this).parent().find(".question-set").last()).fadeIn(400);
	
	// clear out all the values inside input and textarea
	$(this).parent().find(".question-set").last().find("input, textarea").each(function(){
		$(this).val(null);
	});
	// clear the image generated on a div
	$(this).parent().find(".question-set").last().find(".guess_img_holder").each(function(){
		$(this).find(".guess_img_style").remove();
	});
});

// if the system detects a change on textarea for fill in the blanks question set
// $("#fith-question").on("change", "textarea", function(){
// 	var ex = "";
// 	var str = $(this).val();
// 	var strlen = str.length;
// 	var exist = "no";
// 	for(var i = 0; i < (strlen); i++){
// 		// check if the question has __ doubleunderscore and change it to a html element if yes
// 		if(str[i] == "_" && str[i+1] == "_"){
// 			ex += "<span class='blanks_here'></span>";
// 			exist = "yes";
// 		}else if(str[i] != "_"){
// 			ex += str[i];
// 		}
// 	}
// 	if(exist == "yes"){
// 		$(this).val(ex);
// 	}else{
// 		alert("Please add __ (double underscore to create a blank space.)");
// 	}
// });
var usExist = "no";
function changeFillBlanksText(){
	$("#fith-question").find(".emp_question").each(function(){
		if(!$(this).val()){
			alert("You must type a question.");
			$(this).addClass("err");
			setTimeout(function(){
				$(this).removeClass("err");
			},3000);
			return false;
		}else{
			var ex = "";
			var str = $(this).val();
			var strlen = str.length;
			
			for(var i = 0; i < (strlen); i++){
				// check if the question has __ doubleunderscore and change it to a html element if yes
				if(str[i] == "_" && str[i+1] == "_"){
					ex += "<span class='blanks_here'></span>";
					usExist = "yes";
				}else if(str[i] != "_"){
					ex += str[i];
				}
			}
			if(usExist == "yes"){
				submitFillBlanks();
			}else{
				alert("Please add __ (double underscore to create a blank space.)");
			}
		}

	});

}

// Create checkbox element for question set
function makeCheckbox(num, clearSt){

	var o = 
		'<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 grid-op">' +
			'<div class="form-group">' +
				'<label for="">Options / Answer: </label>' +
				'<input type="text" name="opt[]" class="form-control" required>' +
			'</div>' +
		'</div>';

	if(clearSt == "clear"){
		$("#chk-question .chk-options").empty();
		$("#countCheck").val(num);
	}else{
		var maxCount = $(clearSt).parent().siblings('.row').children('.chk-options').find(".grid-op").length;
		if(maxCount < 4){
			// var c = parseInt($(clearSt).parent().parent().siblings("#countCheck").val()) + 1;
			// $(clearSt).parent().parent().siblings("#countCheck").val(c);
			$(o).hide().appendTo($(clearSt).parent().siblings(".row").find(".chk-options")).fadeIn(500);
		}else{
			alert("Cannot Create anymore options.");
		}
		return 1;
	}

	for (var i = 0; i < num; i++) {
		var o = 
		'<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 grid-op">' +
			'<div class="form-group">' +
				'<label for="">Options ' + (i+1) + ': </label>' +
				'<input type="text" name="opt[]" class="form-control" required>' +
			'</div>' +
		'</div>';
		$(o).hide().appendTo("#chk-question .chk-options").fadeIn(400);
	}

	$("#chk-question").fadeIn(500);
	

}

// Create radio element for question set
function makeRadio(num, clearSt){
		
	var o = 
		'<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 grid-op">' +
			'<div class="form-group">' +
				'<label for="">Options / Answer: </label>' +
				'<input type="text" name="opt[]" class="form-control" required>' +
			'</div>' +
		'</div>';

	if(clearSt == "clear"){
		$("#rad-question .rad-options").empty();
	}else{
		var maxCount = $(clearSt).parent().siblings('.row').children('.chk-options').find(".grid-op").length;
		if(maxCount < 4){
			$(o).hide().appendTo($(clearSt).parent().siblings(".row").find(".rad-options")).fadeIn(500);
		}else{
			alert("Cannot Create anymore options.");
		}
		return 1;
	}

	for (var i = 0; i < num; i++) {
		var o = 
		'<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 grid-op">' +
			'<div class="form-group">' +
				'<label for="">Options ' + (i+1) + ' </label>' +
				'<input type="text" name="opt[]" class="form-control" required>' +
			'</div>' +
		'</div>';
		$(o).hide().appendTo("#rad-question .rad-options").fadeIn(400);
	}

	$("#rad-question").fadeIn(500);
}

// Create image blanks for question set
function makeImgBlanks(clearSt){
		
	var o = 
		'<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 grid-op">' +
			'<div class="form-group">' +
				'<label for="">Option: </label>' +
				'<div class="guess_img_holder">' +
				'</div>' + 
				'<input type="file" accept="image/*" class="class_guess_img form-control" name="img[]" required>' +
			'</div>' +
		'</div>';

	if(clearSt == "clear"){
		$("#ig-question .ig-options").empty();
	}else{
		$(o).hide().appendTo($(clearSt).parent().siblings(".row").find(".ig-options")).fadeIn(500);
		return 1;
	}

	
	$(o).hide().appendTo("#ig-question .ig-options").fadeIn(400);
	

	$("#ig-question").fadeIn(500);
}

// Create drag & drop question set
function makeDragDrop(){
		for(var i = 0; i < 3; i++){
			var o = 
				'<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 grid-op">' +
					'<div class="form-group">' +
						'<label for="">Option '+ (i+1) +' </label>' +
						'<div class="guess_img_holder">' +
						'</div>' + 
						'<input type="file" accept="image/*" name="img[]" class="class_guess_img form-control" required>' +
					'</div>' +
				'</div>';
			$(o).hide().appendTo("#idrag-question .idrag-options").fadeIn(400);
		}
				// '<input type="text" name="imgName[]" placeholder="Name of the image" class="form-control class_guess_name" required>' +

	$("#idrag-question").fadeIn(500);
}

// Create Audio question set
function makeAudioFile(){
	var o = 
		'<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 grid-op">' +
			'<div class="form-group">' +
				'<label for="">Choose Audio File (MP3): </label>' +
				'<input type="file" name="audio[]" accept="audio/*" class="form-control">' +
			'</div>' +
		'</div>';
	
	$(o).hide().appendTo("#audio-question .audio-options").fadeIn(400);
		

	$("#audio-question").fadeIn(500);
}