// dynamically added buttons wont generate event so we use modal as a source to listen for any event 
$(".modal").on("click", "button", function(){
	// if the clicked button is for del-option
	if($(this).hasClass("del-option")){
		if($(this).parent().parent().find(".del-option").is(":visible").length > 2){

			// fadeout the option field and remove it.
			$(this).parent().fadeOut(500);
			var d = $(this);
			setTimeout(function(){
				d.parent().hide();
			}, 550);

		}else{
			alert("Will it still be an exam if you delete all options?");
		}

	}

	if($(this).hasClass("del-formQuest")){
		if($(this).parent().parent().parent().find(".question-set").length > 1){
			// fadeout the question set and remove it
			var clas = $(this).parent().parent().fadeOut(400);
			setTimeout(function(){
				clas.remove();
			},500);
		}else{
			alert("Cannot Delete last element.");
		}
	}

	// if the clicked button is for del-mkQuestion
	if($(this).hasClass("del-mkQuest")){
		// check for the length of question set
		if($(this).parent().parent().parent().parent().find(".question-set").length > 1){
			// fadeout the question set and remove it
			var clas = $(this).parent().parent().parent().fadeOut(400);
			setTimeout(function(){
				clas.remove();
			},500);
		}else{
			// in case the question set is the only one remaining 
			alert("Cannot delete last element.");
		}

	}

});