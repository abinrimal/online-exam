// Function to run in case user submits a question form
$(".spec-submit-btn").click(function(){

	// all variable is used to store the data after cleaning up the serialized form
	all = {};
	var index = 0;

	$(this).siblings(".set-container").find("form").each(function(){

		countEmpty = 0;
		var auth = true;

		// Checks the form if any input field is empty
		// checkIfEmpty($(this));


		if(auth == true){

			// serialize all the form fields into an array with json data
			var form = $(this).serializeArray();

			// =========================================================================================

			// for each of the question sets
				all[index] = {};

				// store temporary data from multiple options
				var temp = [];

				for(var i = 0; i < form.length; i++){
					// split [] sign from the string
					// console.log("f_name = " + form[i].name.split("[]")[0] + ", f_value = " + form[i].value);
					var name = form[i].name.split("[]")[0];
					var val = form[i].value;

					// if name is not already inside the object
					if(!(name in all[index])){

						all[index][name] = val;

					}else{
						if(name == "opt"){
							// if the temp array length is 0 and data stored as option is only a string instead of an array
							if(typeof all[index]["opt"] == "string" && temp.length == 0){
								// push the data to temporary data container
								temp.push(all[index]["opt"]);
							}
							// push the current value of option to temporary container
							temp.push(val);
							// assign all temporary data as an array to all object
							all[index][name] = temp;
						}
						if(name == "answer"){
							all[index]['answer'] += "," + val;
						}
					}

				}
				index++;
			// ==================================================================================================
		}
		
	});
	// console.log(all);
	createChk(all);
	submitChk();
});


