var imgForm = new FormData();

$(".ig-submit").click(function(){

	// all variable is used to store the data after cleaning up the serialized form
	all = {};
	var index = 0;

	$(this).siblings(".set-container").find("form").each(function(){

		countEmpty = 0;
		var auth = true;

		// Checks the form if any input field is empty
		checkIfEmpty($(this));

		$(this).find(".class_guess_img").each(function(){
			var file = $(this).prop('files')[0];
			imgForm.append(("img_"+index), file);
		});


		if(auth == true){
			// serialize all the form fields into an array with json data
			var form = $(this).serializeArray();

			// =========================================================================================

			// for each of the question sets
				all[index] = {};

				// store temporary data from multiple options
				var temp = [];

				for(var i = 0; i < form.length; i++){
					// split [] sign from the string
					var name = form[i].name.split("[]")[0];
					var val = form[i].value;

					// if name is not already inside the object
					if(!(name in all[index])){

						all[index][name] = val;

					}

				}

				index++;
			// ==================================================================================================

		}
		
	});
	// console.log(all);
	// createImgGuess(all, imgForm);
	// submitImageGuess();
});
