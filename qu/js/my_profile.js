var w = $(document).innerWidth();
if(w < 1120){
	if(!$("body").hasClass("sidebar-xs")){
		$("body").addClass("sidebar-xs");
	}
}else{
	if($("body").hasClass("sidebar-xs")){
		$("body").removeClass("sidebar-xs");
	}
}

$(window).resize(function(){
	var width = $(document).innerWidth();
	if(width < 1120){
		if(!$("body").hasClass("sidebar-xs")){
			$("body").addClass("sidebar-xs");
		}
	}else{
		if($("body").hasClass("sidebar-xs")){
			$("body").removeClass("sidebar-xs");
		}
	}
});

$('#exit-warning').click(function(){
	swal({
	  title: "Are you sure?",
	  text: "Any changes made will not be saved!",
	  icon: "warning",
	  buttons: true,
	  dangerMode: true,
	}).then((value) => {
		alert(value);
	});
});



// Variables that holds class name
var fn = '.first_name';
var ln = '.last_name';
var email = '.email';
var country = '.country';
var address = '.address';
var mobile_no = '.mobile';
var secondary_school = '.secondary_school';
var high_school = '.high_school';
var bachelors_degree = '.bachelors_degree';

// Json format data
var details = {
	'first_name': 'Angnima',
	'last_name': 'Sherpa',
	'email': 'angnimasherpa7@gmail.com',
	'gender': 'male',
	'country': 'Nepal',
	'address': 'Kathmandu, Boudha 06',
	'mobile_no': '9818810378',
	'secondary_school': 'Kumari English Boarding School',
	'high_school': 'GoldenGate International College',
	'bachelors_degree': 'Herald College'
}

// Appending data to individual input fields
function formData(data, type){

	if(type == 'print'){
		// Print the data to user's profile details
		$(fn + data).text(details.first_name);
		$(ln + data).text(details.last_name);
		$(email + data).text(details.email);
		$(country + data).text(details.country);
		$(address + data).text(details.address);
		$(mobile_no + data).text(details.mobile_no);
		$(secondary_school + data).text(details.secondary_school);
		$(high_school + data).text(details.high_school);
		$(bachelors_degree + data).text(details.bachelors_degree);
		$('.gender-p').text(details.gender);

	}else{

		// Append data as a value to the input fields 
		$(fn + data).val(details.first_name);
		$(ln + data).val(details.last_name);
		$(email + data).val(details.email);
		$(country + data).val(details.country);
		$(address + data).val(details.address);
		$(mobile_no + data).val(details.mobile_no);
		$(secondary_school + data).val(details.secondary_school);
		$(high_school + data).val(details.high_school);
		$(bachelors_degree + data).val(details.bachelors_degree);

		// Condition for gender
		if(details.gender == 'male'){
			$('.male-here').prop('checked', true);
		}else if(details.gender == 'female'){
			$('.female-here').prop('checked', true);
		}else{
			$('.other_gender-here').prop('checked', true);
		}

	}
}

// Start appending data to user's profile on load of document
function start(){
	formData('-p', 'print');
}

start();

// Append data to input fields only in edit mode
function editMode(){
	formData('-here', 'input');
}

// Disable input fields of form
function disabledFields(){
	$('form input').prop('disabled', true);
	$('form .btn').prop('disabled', true);
}
disabledFields();

// Enable input fields of form on edit mode 
function enableFields(){
	$('form input').prop('disabled', false);
	$('form .btn').prop('disabled', false);
	$('form .password-here').prop('disabled', true);
}

function exitEditMode(){
	var con = confirm("Do you want to exit edit mode? All your changes will be reset.");
	if(con){
		return true;
	}else{
		return false;
	}
}

// Warning confirm alert
function warningAlert(){
	swal({
	  title: "Are you sure?",
	  text: "Any changes made will not be saved!",
	  icon: "warning",
	  buttons: true,
	  dangerMode: true,
	}).then((value) => {
		if(value == true){

			$('#edit-profile-btn').removeClass('is-on');
			$('#edit-profile-btn').html("<i class='fa fa-edit'></i>'");
			$('#user-form-details').hide();
			$('#static-profile').fadeIn(400);

			disabledFields();

		}
	});
}

// Floating edit button onclick event 
$('#edit-profile-btn').click(function(){
	if($(this).hasClass('is-on')){
		warningAlert();
	}else{
		$(this).addClass('is-on');
		$(this).html("&times");
		$('#static-profile').hide();
		$('#user-form-details').fadeIn(400);

		editMode();
		enableFields();
	}
});

// Cancle button onclick event
$('#cancle-edit').click(function(){
	warningAlert();
});

// Form submit event
$('form').on('submit', function(e){
	e.preventDefault();

});