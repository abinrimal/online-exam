$(document).ready(function(){

	// change the sidebar setting in case document width is less than 1120px
	var w = $(document).innerWidth();
	if(w < 1120){
		if(!$("body").hasClass("sidebar-xs")){
			$("body").addClass("sidebar-xs");
		}
		if($("body").hasClass("sidebar-xs")){
			$(".sidebar-xs .header-highlight .navbar-header .navbar-brand").css({
				"background": "url('ex_logo.png')",
				"background-size": "40px",
				"background-repeat": "no-repeat"
			});
			$(".sidebar-xs .header-highlight .navbar-header .navbar-brand").text(" ");
		}
	}else{
		if($("body").hasClass("sidebar-xs")){
			$("body").removeClass("sidebar-xs");
			$(".sidebar-xs .header-highlight .navbar-header .navbar-brand").css("background","");
			$(".sidebar-xs .header-highlight .navbar-header .navbar-brand").text(" Online Exam ");
		}
	}

	var hasImage = true;
	// change the sidebar setting on window resize
	$(window).resize(function(){
		var width = $(document).innerWidth();
		if(width < 1120){
			if(!$("body").hasClass("sidebar-xs")){
				$("body").addClass("sidebar-xs");
			}
			if(!hasImage){
				if($("body").hasClass("sidebar-xs")){
					$(".sidebar-xs .header-highlight .navbar-header .navbar-brand").css({
						"background": "url('ex_logo.png')",
						"background-size": "40px",
						"background-repeat": "no-repeat"
					});
				}
				$(".sidebar-xs .header-highlight .navbar-header .navbar-brand").text(" ");
			}
		}else{
			if($("body").hasClass("sidebar-xs")){
				$("body").removeClass("sidebar-xs");
				$(".sidebar-xs .header-highlight .navbar-header .navbar-brand").css("background","");
				$(".sidebar-xs .header-highlight .navbar-header .navbar-brand").text(" Online Exam ");
			}
		}
	});
});