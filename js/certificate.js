let signLength = $('.certi-signatures').find('.sin-signature').length;
if(signLength == 1){
	$('.sin-signature').css({
		width: '300px',
		margin: 'auto'
	});
}else if(signLength == 2){
	$('.certi-signatures .row').prepend('<div class="col-lg-1 col-md-1 col-sm-1" style="margin-right:50px;"></div>');
	$('.sin-signature').first().addClass('col-lg-3 col-md-3 col-sm-3');
	$('.sin-signature').last().addClass('col-lg-3 col-lg-offset-3 col-md-3 col-md-offset-3 col-sm-3 col-sm-offset-3');
}else if(signLength == 3){
	$('.certi-signatures .row').prepend('<div class="col-lg-1 col-md-1 col-sm-1" style="margin-right:50px;"></div>');
	$('.sin-signature').addClass('col-lg-3 col-md-3 col-sm-3');
}else if(signLength > 3){
	$('.sin-signature').addClass('col-lg-3 col-md-3 col-sm-3');
}