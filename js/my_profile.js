var w = $(document).innerWidth();
if(w < 1120){
	if(!$("body").hasClass("sidebar-xs")){
		$("body").addClass("sidebar-xs");
	}
}else{
	if($("body").hasClass("sidebar-xs")){
		$("body").removeClass("sidebar-xs");
	}
}

$(window).resize(function(){
	var width = $(document).innerWidth();
	if(width < 1120){
		if(!$("body").hasClass("sidebar-xs")){
			$("body").addClass("sidebar-xs");
		}
	}else{
		if($("body").hasClass("sidebar-xs")){
			$("body").removeClass("sidebar-xs");
		}
	}
});

$('#exit-warning').click(function(){
	swal({
	  title: "Are you sure?",
	  text: "Any changes made will not be saved!",
	  icon: "warning",
	  buttons: true,
	  dangerMode: true,
	}).then((value) => {
		alert(value);
	});
});


// Disable input fields of form
function disabledFields(){
	$('form input').prop('disabled', true);
	$('form .btn').prop('disabled', true);
}
disabledFields();

// Enable input fields of form on edit mode 
function enableFields(){
	$('form input').prop('disabled', false);
	$('form .btn').prop('disabled', false);
	$('form .password-here').prop('disabled', true);
}

function exitEditMode(){
	var con = confirm("Do you want to exit edit mode? All your changes will be reset.");
	if(con){
		return true;
	}else{
		return false;
	}
}

// Warning confirm alert
function warningAlert(){
	swal({
	  title: "Are you sure?",
	  text: "Any changes made will not be saved!",
	  icon: "warning",
	  buttons: true,
	  dangerMode: true,
	}).then((value) => {
		if(value == true){

			$('#edit-profile-btn').removeClass('is-on');
			$('#edit-profile-btn').html("<i class='fa fa-edit'></i>'");
			$('#user-form-details').hide();
			$('#static-profile').fadeIn(400);

			disabledFields();

		}
	});
}

// Floating edit button onclick event 
$('#edit-profile-btn').click(function(){
	if($(this).hasClass('is-on')){
		warningAlert();
	}else{
		$(this).addClass('is-on');
		$(this).html("&times");
		$('#static-profile').hide();
		$('#user-form-details').fadeIn(400);

		enableFields();
	}
});

// Cancle button onclick event
$('#cancle-edit').click(function(){
	warningAlert();
});

// Form submit event
$('form').on('submit', function(e){
	

});